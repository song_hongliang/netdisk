create database if not exists netdisk character set utf8;
use netdisk;
create table file
(
    id varchar(100) not null comment '文件编号'
        primary key,
    filename varchar(100) null comment '文件名',
    filePath varchar(1000) null comment '文件存储路径（展示用）',
    fileOwner varchar(100) null comment '文件所有者',
    fileType varchar(100) null comment '文件类别',
    del varchar(10) null comment '是否删除',
    openFile varchar(10) null comment '文件是否公开',
    createTime timestamp default CURRENT_TIMESTAMP null comment '文件创建时间',
    realPath varchar(100) null comment '实际存储路径',
    fileNum int default 1 null comment '文件数量',
    openTime timestamp null comment '文件公开时间'
)
    charset=utf8;

create table share
(
    id varchar(100) not null comment '分享编号'
        primary key,
    shareMan varchar(100) not null comment '分享者',
    shareFileId varchar(100) not null comment '被分享的文件编号',
    shareT int not null comment '分享链接有效时长',
    shareP varchar(30) not null comment '分享密码'
)
    charset=utf8;

create table user
(
    id varchar(100) not null comment '用户id'
        primary key,
    username varchar(100) not null comment '用户名',
    password varchar(100) not null comment '密码',
    level varchar(100) null comment '用户类别'
)
    charset=utf8;

commit;
