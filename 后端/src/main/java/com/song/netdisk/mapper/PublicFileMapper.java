package com.song.netdisk.mapper;

import com.song.netdisk.model.UserFile;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PublicFileMapper {

    /*查找全部公开文件*/
    List<UserFile> getAllPublicFile();

    /*公开一个文件*/
    int openFile(String fileId, String userId);

    /*查看个人公开文件*/
    List<UserFile> getMyFile(String userId);

    /*最近公开的文件*/
    List<UserFile> getRecentlyOpenFile();

    List<UserFile> searchPublicFile(String msg);

    int closeFile(String id, String userId);
}
