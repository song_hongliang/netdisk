package com.song.netdisk.mapper;

import com.song.netdisk.model.Share;
import com.song.netdisk.model.UserFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ShareMapper {

    //    新建分享链接
    int createShare(Share share);

    //    查询特定用户的全部分享链接
    List<Share> getUserShare(String userid);

    //    更改有效时长
    int updateTime(@Param("ids") List<String> ids);

    //    遍历整张share表，修改有效时间
    int updateAllTime();

    //    删除链接,包含到期删除和用户自主删除
    int delShare(@Param("ids") List<String> ids);

    //    根据编号查询share
    Share getShareById(String id);

    //      根据文件编号查询分享信息
    Share getShareByFile(String fileId);

    //    根据share俩编号获取文件信息
    UserFile getFileByShare(Share share);

    //    根据share编号将有效提取次数 -1
    void updateShareTById(String id);

    //  根据share的id删除
    void delShareById(String id);

//    文件要删了，对应的分享链接也要删了
    int delShareByDelFile(String fileId, String userid);
}
