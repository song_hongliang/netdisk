package com.song.netdisk.mapper;

import com.song.netdisk.model.UserFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FileMapper {

//    上传文件
    Integer uploadFile(UserFile userFile);

//    删除文件(包含批量删除)
    Integer delFile(@Param("ids") List<String> ids);

//    删除文件(一个)
    Integer delOneFile(String fileId);

//    查看目录下的全部文件
    List<UserFile> getCurrentPositionFile(String userid, String path);

//    改变文件的保存位
    Integer changeDel(String id, String msg);

    // 搜索用户文件
    List<UserFile> searchUserFile(String fileOwner, String filename);

//    批量回收
    int fakeDel(@Param("ids") List<String> ids);

//    获取暂时删除的文件
    List<UserFile> getDelFile(String id);

//    根据文件编号获取文件信息（批量）
    List<UserFile> getFilesById(@Param("ids") List<String> ids);

//    根据文件编号获取文件信息（单个）
    UserFile getFileById(String fileId);

    List<UserFile> getAllUserFolder(String userId);

    String haveCodeFile(UserFile userFile);

    int saveCodeFile(UserFile userFile);

//    使文件数 +1
    int updateFileCount(String realPath);

//    使文件数 -1
    int minusFileCount(String realPath);

//    获取用户文件
    UserFile getFolderByNameAndPath(String filePath, String userid);

    String getIoFilePath(String fileId, String userId);

    int getFileNameCount(String fileName, String filePath, String fileOwner);

    UserFile getFileRealPath(String fileId);
}
