package com.song.netdisk.mapper;

import com.song.netdisk.model.UserFile;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdminMapper {

    List<UserFile> getAllUserFiles();

    int deleteFile(String id);

    int addAdmin(String adminPost, String adminName);
}
