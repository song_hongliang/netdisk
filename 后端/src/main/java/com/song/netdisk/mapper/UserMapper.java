package com.song.netdisk.mapper;

import com.song.netdisk.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

//    login
    String login(User user);

//    regist
    int regist(User user);

//    getUsername
    String getUsername(String id);

//    列出全部用户，仅管理员可以用此接口
    List<User> getAllUser();

//   获取该用户名的个数
    int getUserNameCount(String msg);
}
