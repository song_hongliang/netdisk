package com.song.netdisk.aop;

import com.song.netdisk.model.UserFile;
import com.song.netdisk.util.JwtUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.List;

@Aspect
@Component
public  class SetFileToken {

    @Pointcut("@annotation(org.springframework.web.bind.annotation.PostMapping) || @annotation(org.springframework.web.bind.annotation.GetMapping)")
    public  void getRes(){}

    @Around("getRes()")
    public Object addTokenToRealPath(ProceedingJoinPoint point){
        Object[] args = point.getArgs();
        Object proceed = null;
        try{
            proceed = point.proceed(args);
            if(proceed instanceof List){
                List objectList = (List) proceed;
                for (Object o : objectList) {
                    if(o instanceof UserFile){
                        setPathToken(o);
                    }
                }
            }
            else if(proceed instanceof UserFile){
                setPathToken(proceed);
            }
        }catch (Throwable e){
            e.printStackTrace();
        }
        return proceed;
    }

    private void setPathToken(Object o){
        Class<?> aClass = o.getClass();
        for (Field declaredField : aClass.getDeclaredFields()) {
            declaredField.setAccessible(true);
            Object _v = null;
            try {
                _v = declaredField.get(o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if(declaredField.getName().equals("realPath")){
                String realPath = (String) _v;
                realPath += "?access=" + JwtUtil.setFileAccess(realPath);
                try {
                    declaredField.set(o, realPath);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
