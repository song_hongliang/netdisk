package com.song.netdisk.service;

import com.song.netdisk.mapper.AdminMapper;
import com.song.netdisk.mapper.FileMapper;
import com.song.netdisk.mapper.UserMapper;
import com.song.netdisk.model.UserFile;
import com.song.netdisk.util.Tool;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminService {
    int hasRegisted = 0;

    @Value("${adminPost}")
    private String adminPost;

    @Value("${savePath}")
    private String localPath;

    @Resource
    private AdminMapper adminMapper;

    @Resource
    private UserMapper userMapper;

    @Resource
    private FileMapper fileMapper;

    private boolean isAdmin(ServletRequest request) {
        String userId = Tool.getUserId(request);
        return adminPost.equals(userId);
    }

    @Transactional
    public String deleteFile(String fileId, ServletRequest request) {
        if (isAdmin(request)) {
            //  文件物理删除
            UserFile fileById = fileMapper.getFileById(fileId);
            File file = new File(localPath + "/" + fileById.getRealPath());
            if(file.delete()){
                adminMapper.deleteFile(fileId);
                return "success";
            }
            return "failed";

        }
        return "not allowed admin";

    }

    public List<UserFile> getAllFiles(ServletRequest request) {
        if (isAdmin(request)){
            return adminMapper.getAllUserFiles().stream().filter(item-> item.getRealPath().contains(item.getFileOwner())).collect(Collectors.toList());
        }

        return null;
    }

    /*项目初始化时运行的添加admin方法*/
    public void addAdmin(String adminPost, String adminName) {
        if (hasRegisted == 1)
            return;
        int userNameCount = userMapper.getUserNameCount(adminName);
        if (userNameCount < 1) {
            adminMapper.addAdmin(adminPost, adminName);
            hasRegisted++;
        }
    }
}
