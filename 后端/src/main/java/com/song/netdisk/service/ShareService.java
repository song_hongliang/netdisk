package com.song.netdisk.service;

import com.song.netdisk.mapper.ShareMapper;
import com.song.netdisk.model.Content;
import com.song.netdisk.model.Share;
import com.song.netdisk.model.User;
import com.song.netdisk.model.UserFile;
import com.song.netdisk.util.Tool;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ShareService {

    @Resource
    private ShareMapper shareMapper;

    @Transactional
    public Share createShare(Share share) {
        String id = Tool.getUUid();
        share.setId(id);
        if (share.getShareP().equals(""))
            share.setShareP(Tool.getRandomCode());
        if (share.getShareT() == null)
            share.setShareT(1);
        shareMapper.createShare(share);
        return shareMapper.getShareById(id);
    }

    public List<Share> getUserShare(String userid) {
        return shareMapper.getUserShare(userid);
    }

    public String updateTime(List<String> ids) {
        int lines = shareMapper.updateTime(ids);
        if (lines > 0)
            return Content.success;
        return Content.failed;
    }

    public String delShare(List<String> ids) {
        int lines = shareMapper.delShare(ids);
        if (lines > 0)
            return Content.success;
        return Content.failed;
    }

    public void updateAllTime() {
        shareMapper.updateAllTime();
    }

    //    根据文件编号获取分享信息
    public Share getShareByFile(String fileId) {
        return shareMapper.getShareByFile(fileId);
    }

    //    根据传入的两个码获取文件编号,返回文件信息以及剩余的有效提取次数
    public Map<String, Object> getFileByShare(Share share, String userid) {
        Map<String, Object> resMap = new HashMap<>();
        Share res = shareMapper.getShareById(share.getId());
        if (res == null)
            return null;
        UserFile fileByShare = shareMapper.getFileByShare(res);
        //        登录人和分享人是同一个。
        if (userid.equals(res.getShareMan())) {
            resMap.put("path", fileByShare.getFilePath());
        }
//        其他情况视为有效提取
        else {
            if (fileByShare != null) {
                resMap.put("file", fileByShare);
                //        剩余有效提取次数 >1
                if (res.getShareT() > 1){
                    shareMapper.updateShareTById(share.getId());
                    resMap.put("times", res.getShareT() - 1);
                }

                //        只能提取一次了,则在提取完成后删除链接。
                else {
                    shareMapper.delShareById(share.getId());
                    resMap.put("times", 0);
                }
            }
        }
        return resMap;
    }
}
