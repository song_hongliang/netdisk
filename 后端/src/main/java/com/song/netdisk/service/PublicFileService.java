package com.song.netdisk.service;

import com.song.netdisk.mapper.PublicFileMapper;
import com.song.netdisk.mapper.UserMapper;
import com.song.netdisk.model.Content;
import com.song.netdisk.model.UserFile;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PublicFileService {

    @Resource
    private PublicFileMapper publicFileMapper;

    @Resource
    private UserMapper userMapper;

    public List<UserFile> getAllPublicFile() {
        List<UserFile> allPublicFile = publicFileMapper.getAllPublicFile();
        allPublicFile.forEach(item->{
            item.setFileOwnerName(userMapper.getUsername(item.getFileOwner()));
        });
        return allPublicFile;
    }

    public List<UserFile> getRecentlyOpenFile(){
        List<UserFile> recentlyOpenFile = publicFileMapper.getRecentlyOpenFile();
        recentlyOpenFile.forEach(item->{
            item.setFileOwnerName(userMapper.getUsername(item.getFileOwner()));
        });
        return recentlyOpenFile;
    }

    /*数据库调了一下只有原上传者可以公布自己的文件*/
    public String openFile(String fileId, String userId) {
        int lines = publicFileMapper.openFile(fileId, userId);
        return lines == 1? Content.success: Content.failed;
    }

    /*获取个人公开文件*/
    public List<UserFile> getMyFile(String userId) {
        return publicFileMapper.getMyFile(userId);
    }

    public List<UserFile> searchPublicFile(String msg) {
        List<UserFile> userFiles = publicFileMapper.searchPublicFile(msg);
        userFiles.forEach(item->{
            item.setFileOwnerName(userMapper.getUsername(item.getFileOwner()));
        });
        return userFiles;
    }

    public String closeFile(String id, String userId) {
        int lines = publicFileMapper.closeFile(id, userId);
        return lines == 1? Content.success: Content.failed;
    }
}
