package com.song.netdisk.service;

import com.song.netdisk.mapper.UserMapper;
import com.song.netdisk.model.Content;
import com.song.netdisk.model.User;
import com.song.netdisk.model.UserFile;
import com.song.netdisk.util.JwtUtil;
import com.song.netdisk.util.Tool;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserService {

//    文件存储路径
    @Value("${savePath}")
    private String localPath;

    @Resource
    private UserMapper userMapper;

    @Resource
    private FileService fileService;

    //    login
    public Map<String, String> login(User user) {
        String s = Tool.HidPass(user.getPassword());
        user.setPassword(s);
        String userid = userMapper.login(user);
        String token = null;
        if(userid != null){
            token = JwtUtil.createToken(user.getUsername(), userid);
        }
        Map<String, String> res = new HashMap<>();
        res.put("userid", userid);
        res.put("token", token);
        return res;
    }

    //    regist
    public String regist(User user) {
        if(user.getUsername() == null || user.getUsername().equals(""))
            return Content.failed;
        if(user.getPassword() == null || user.getPassword().equals(""))
            return Content.failed;
//        建立用户信息
        if (userMapper.getUserNameCount(user.getUsername()) != 0)
            return Content.failed;
        String s = Tool.HidPass(user.getPassword());
        user.setPassword(s);
        String userid = Tool.getUUid();
        user.setId(userid);

//        创建个人目录
        File file = new File(localPath + "/" + userid);
        file.mkdir();

//        存入数据表
        UserFile userFile = new UserFile();
        userFile.setId(userid);
        userFile.setFileOwner(userid);
        userFile.setFilename(userid);
        userFile.setFileType(Content.folder);
        userFile.setDel(Content.rootFile);
        fileService.uploadFile(userFile);

        if (userMapper.regist(user) == 1)
            return Content.success;
        return Content.failed;
    }

    //    getUsername
    public String getUsername(String id) {
        return userMapper.getUsername(id);
    }
}
