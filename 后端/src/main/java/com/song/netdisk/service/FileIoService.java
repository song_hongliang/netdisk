package com.song.netdisk.service;

import com.song.netdisk.util.Tool;
import com.spire.presentation.Presentation;
import lombok.extern.slf4j.Slf4j;
import org.docx4j.Docx4J;
import org.docx4j.convert.out.FOSettings;
import org.docx4j.fonts.IdentityPlusMapper;
import org.docx4j.fonts.Mapper;
import org.docx4j.fonts.PhysicalFonts;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.*;

@Service
@Slf4j
public class FileIoService {

    @Value("${savePath}")
    private String filePath;

    public void getFileIo(String realPath, ServletResponse response) {
        String ioFilePath = filePath + "/" + realPath;
        try {
            FileCopyUtils.copy(new FileInputStream(ioFilePath), response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //    word转换为pdf后存储到本地
    public String wordToPdf(String realPath, ServletRequest request) {
        String userid = Tool.getUserId(request);
        if(realPath.contains("?")){
            realPath = realPath.split("\\?")[0];
        }
        String ioFilePath = filePath + "/" + realPath;

        OutputStream os;
        InputStream in;
        try {
            in = new FileInputStream(ioFilePath);
            WordprocessingMLPackage mlPackage = Docx4J.load(in);
            Mapper fontMapper = new IdentityPlusMapper();
            fontMapper.put("Times New Roman", PhysicalFonts.get("Arial Unicode MS"));
            fontMapper.put("隶书", PhysicalFonts.get("LiSu"));
            fontMapper.put("宋体", PhysicalFonts.get("SimSun"));
            fontMapper.put("微软雅黑", PhysicalFonts.get("Microsoft Yahei"));
            fontMapper.put("黑体", PhysicalFonts.get("SimHei"));
            fontMapper.put("楷体", PhysicalFonts.get("KaiTi"));
            fontMapper.put("新宋体", PhysicalFonts.get("NSimSun"));
            fontMapper.put("华文行楷", PhysicalFonts.get("STXingkai"));
            fontMapper.put("华文仿宋", PhysicalFonts.get("STFangsong"));
            fontMapper.put("仿宋", PhysicalFonts.get("FangSong"));
            fontMapper.put("幼圆", PhysicalFonts.get("YouYuan"));
            fontMapper.put("华文宋体", PhysicalFonts.get("STSong"));
            fontMapper.put("华文中宋", PhysicalFonts.get("STZhongsong"));
            fontMapper.put("等线", PhysicalFonts.get("SimSun"));
            fontMapper.put("等线 Light", PhysicalFonts.get("SimSun"));
            fontMapper.put("华文琥珀", PhysicalFonts.get("STHupo"));
            fontMapper.put("华文隶书", PhysicalFonts.get("STLiti"));
            fontMapper.put("华文新魏", PhysicalFonts.get("STXinwei"));
            fontMapper.put("华文彩云", PhysicalFonts.get("STCaiyun"));
            fontMapper.put("方正姚体", PhysicalFonts.get("FZYaoti"));
            fontMapper.put("方正舒体", PhysicalFonts.get("FZShuTi"));
            fontMapper.put("华文细黑", PhysicalFonts.get("STXihei"));
            fontMapper.put("宋体扩展", PhysicalFonts.get("simsun-extB"));
            fontMapper.put("仿宋_GB2312", PhysicalFonts.get("FangSong_GB2312"));
            fontMapper.put("新細明體", PhysicalFonts.get("SimSun"));
            fontMapper.put("Times-Roman", PhysicalFonts.get(""));
            mlPackage.setFontMapper(fontMapper);
            os = new FileOutputStream(filePath + "/" + userid + "/view.pdf");
            FOSettings foSettings = Docx4J.createFOSettings();
            foSettings.setWmlPackage(mlPackage);
            Docx4J.toFO(foSettings, os, Docx4J.FLAG_EXPORT_PREFER_XSL);
            in.close();
            os.close();
        } catch (Exception e) {
            log.error(e.toString());
        }
        return userid + "/view.pdf";
    }

    public String ppt_xToPdf(String realPath, ServletRequest request) {
        String userid = Tool.getUserId(request);

        if(realPath.contains("?")){
            realPath = realPath.split("\\?")[0];
        }
        String ioFilePath = filePath + "/" + realPath;
        Presentation ppt = new Presentation();
        try {
            ppt.loadFromFile(ioFilePath);
            ppt.saveToFile(filePath + "/" + userid + "/view.pdf", com.spire.presentation.FileFormat.PDF);
        } catch (Exception e) {
            log.error(e.toString());
        }
        ppt.dispose();
        return userid + "/view.pdf";
    }
}
