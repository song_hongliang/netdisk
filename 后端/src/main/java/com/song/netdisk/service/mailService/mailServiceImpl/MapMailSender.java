package com.song.netdisk.service.mailService.mailServiceImpl;

import com.song.netdisk.service.mailService.MailSender;
import com.song.netdisk.util.Tool;
import lombok.Data;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service("mapMailSender")
public class MapMailSender extends MailSender {

    private final int maxTime = 5;

    @Data
    static class DailMail{
//        当日的首封邮件发送时间
        private Date startDate;

//        当前验证码
        private String code;
//        当前验证码的失效时间
        private Date failTime;
//        当日的验证次数
        private Integer times;
    }
    private Map<String, DailMail> mails = new ConcurrentHashMap<>();

//    是否发送邮件
    private boolean ableToSendMail(String address){
        Date now = new Date();
        DailMail dailMail = mails.get(address);

//        首次发送邮件
        if (dailMail == null){
            dailMail = new DailMail();
            dailMail.setTimes(0);
            dailMail.setStartDate(now);
            dailMail.setFailTime(new Date(now.getTime()+1000*60));
            dailMail.setCode(Tool.getRandomCode());
            mails.put(address, dailMail);
            return true;
        }

        if(dailMail.getTimes() == maxTime){
//            超过一天
            if(now.getTime() - dailMail.getStartDate().getTime() > 1000*60*60*24){
                dailMail.setTimes(0);
                dailMail.setStartDate(now);
                dailMail.setCode(Tool.getRandomCode());
                return true;
            }
            return false;
        }else {
            dailMail.setTimes(dailMail.getTimes() + 1);
            dailMail.setCode(Tool.getRandomCode());
            dailMail.setFailTime(new Date(now.getTime()+1000*60));
            return true;
        }

    }

//    发送邮件
    @Override
    public String sendMail(String address){
        String reg = "\\w+[\\w]*@[\\w]+\\.[\\w]+$";
        if(!address.matches(reg)){
            return "非法的邮件格式";
        }
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(mailUsername);
        mailMessage.setTo(address);
        mailMessage.setSubject("关于邮箱验证");
        if(ableToSendMail(address)){
            mailMessage.setText("本次的验证码为"+ mails.get(address).getCode() +"。该验证码在一分钟内有效。每日只有" + maxTime + "次尝试次数");
        }else
            mailMessage.setText("今日验证次数已达" + maxTime + "次上限，请在24小时后重新尝试");
        mailSender.send(mailMessage);
        return "邮件已发送";
    }

    @Override
    public boolean verifyCode(String code, String address){
        DailMail dailMail = mails.get(address);
        if (dailMail == null)
            return false;
        if(!dailMail.getCode().equals(code))
            return false;
        return !dailMail.getFailTime().before(new Date());
    }

    @Override
    public Integer getMailCounts(String address) {
        DailMail dailMail = mails.get(address);
        return dailMail.getTimes();
    }
}
