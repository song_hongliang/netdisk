package com.song.netdisk.service;
/*
 * 关于文件的删除：
 *   由于分享机制的引入，分享后的文件其实并未产生物理内存上的转移，
 *   只是在数据库中将两个虚拟的地址指向同一个文件而已
 *   因此文件做了个个数统计。分享保存会使文件数量加一
 *   文件数量实际归零后（即所有文件持有者都选择删除自己持有的文件）就会触发文件的物理删除
 *   因为回收站机制的引入……
 * 关于文件分享：
 *   目前的打算是先做了文件分享并关联保存机制，然后再实现删除功能。
 * */

import com.song.netdisk.mapper.FileMapper;
import com.song.netdisk.mapper.ShareMapper;
import com.song.netdisk.model.Content;
import com.song.netdisk.model.UserFile;
import com.song.netdisk.util.Tool;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileService {

    @Value("${savePath}")
    private String localPath;

    @Resource
    private FileMapper fileMapper;

    @Resource
    private ShareMapper shareMapper;

    //    递归找文件(根据传入的文件编号查询)(目前就是给删除文件夹使用的)
    private List<String> getFileIds(List<String> ids) {
        List<String> res = new ArrayList<>();
        fileMapper.getFilesById(ids).forEach(item -> {
            res.add(item.getId());
//            处理文件夹
            if (item.getFileType().equals(Content.folder)) {
                List<String> folderFileIds = getCurrentPositionFile(item.getFileOwner(), item.getFilePath() + "/" + item.getFilename()).stream().map(UserFile::getId).collect(Collectors.toList());
                if (folderFileIds.size() > 0) {
                    List<String> fileIds = getFileIds(folderFileIds);
                    res.addAll(fileIds);
                }
            }
        });
        return res;
    }

    //    新建文件夹
    public String uploadFile(UserFile userFile) {
        if (userFile.getFilename().equals("根目录"))
            return Content.failed;
        userFile.setId(Tool.getUUid());
        userFile.setDel(Content.saveFile);
        userFile.setFileType(Content.folder);
        userFile.setRealPath("folder");
        if (fileMapper.uploadFile(userFile) == 1)
            return Content.success;
        return Content.failed;
    }

    /*
     * 1 更新文件数量
     * 2 检测文件数量，将num归零的从空间上删除
     * */
    @Transactional
    public String delFile(List<UserFile> userFiles, String userid) {
        List<String> collect = userFiles.stream().map(UserFile::getId).collect(Collectors.toList());
        List<UserFile> filesById = fileMapper.getFilesById(collect);
        filesById.forEach(item -> {
//            发起删除人清除自身表项
            fileMapper.delOneFile(item.getId());
//            删除其相关的分享链接
            shareMapper.delShareByDelFile(item.getId(), userid);
            if (!item.getFileType().equals(Content.folder) && item.getFileNum() == 1) {
                //      物理删除
                File file = new File(localPath + "/" + item.getRealPath());
                file.delete();
            } else {
//                文件副本数 - 1
                fileMapper.minusFileCount(item.getRealPath());
            }
        });
//        删除文件
        fileMapper.delFile(collect);
//        更新文件数量
        return Content.success;
    }

    //    查看目录下的全部文件
    public List<UserFile> getCurrentPositionFile(String userid, String path) {
        return fileMapper.getCurrentPositionFile(userid, path);
    }

    //    修改文件的删除位状态
    public String changeDel(String id, String msg) {
        if (fileMapper.changeDel(id, msg) == 1)
            return Content.success;
        return Content.failed;
    }

    //    保存文件
    @Transactional
    public String receiveFileAndForm(MultipartFile multipartFile, UserFile userFile) {
        String fileName = multipartFile.getOriginalFilename();
        assert fileName != null;
        String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
        String uuid = Tool.getUUid();
        //                存储在表中的地址(目录)
        String filePath = userFile.getFilePath();
        String filePre = fileName.substring(0, fileName.lastIndexOf("."));
        int number = fileMapper.getFileNameCount(filePre, filePath, userFile.getFileOwner());
        if(number > 0){
            fileName = filePre +"("+number+")."+fileType;
        }
//        实际的存储的物理地址
        String realPath = userFile.getFileOwner() + "/" + uuid + "." + fileType;
        String savePath = localPath + "/" + realPath;
        userFile.setId(uuid);
        userFile.setFilename(fileName);
        userFile.setFileType(fileType);
        userFile.setFilePath(filePath);
        userFile.setRealPath(realPath);
        userFile.setDel(Content.saveFile);
        fileMapper.uploadFile(userFile);

//        存储到物理存储
        File file = new File(savePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Content.success;
    }

    //    根据文件所有者和文件名获取文件（文件夹）
    public List<UserFile> searchUserFile(String fileOwner, String filename) {
        if (filename == null || filename.equals(""))
            return fileMapper.getCurrentPositionFile(fileOwner, fileOwner);
        return fileMapper.searchUserFile(fileOwner, "%" + filename + "%");
    }

    //    逻辑删除
    public String fakeDel(List<String> ids) {
        int lines = fileMapper.fakeDel(getFileIds(ids));
        if (lines == 0)
            return Content.failed;
        return Content.success;
    }

    //    还原文件。要向上搜索文件夹并修改其状态
    public String livFile(String fileId, String userid) {
        UserFile file = fileMapper.getFileById(fileId);
        fileMapper.changeDel(fileId, Content.saveFile);
        String filePath = file.getFilePath();
        //    在还有被删除的父级目录的情况下再次调用自身
        if (filePath.contains("/")) {
            filePath = filePath.substring(0, filePath.lastIndexOf("/"));
            UserFile folder = fileMapper.getFolderByNameAndPath(filePath, userid);
            if (folder.getDel().equals(Content.delFile)) {
                livFile(folder.getId(), userid);
            } else {
                return Content.success;
            }
        }
        return Content.success;
    }

    //    获取逻辑删除的文件
    public List<UserFile> getDelFile(String id) {
        return fileMapper.getDelFile(id);
    }

    //    返回用户的所有文件夹
    public List<UserFile> getAllUserFolder(String userId) {
        return fileMapper.getAllUserFolder(userId);
    }

    public String saveShareFile(UserFile userFile) {
        Integer lines = fileMapper.uploadFile(userFile);
        return lines == 1 ? Content.success : Content.failed;
    }

    //    根据saveRoot判断存哪去了
//    还得加个判断，如果文件已经存储过了就不要再保存一次了
    @Transactional
    public String saveCodeFile(UserFile userFile, ServletRequest request) {
        String userid = Tool.getUserId(request);
        userFile.setFileOwner(userid);
        if("null/根目录".equals(userFile.getFilePath()))
            userFile.setSaveRoot(true);
//        如果发现当前登录用户已经存储过就不保存
        String filePath = fileMapper.haveCodeFile(userFile);
        if (filePath != null)
            return filePath;

        userFile.setId(Tool.getUUid());
        if (userFile.getSaveRoot()) {
            userFile.setFilePath(userid);
        }

        fileMapper.saveCodeFile(userFile);
        //        存储后会导致文件副本增加，这将会对删除产生影响因此需要更新文件总数
        int lines = updateFileCount(userFile.getRealPath());
        return lines == userFile.getFileNum() + 1 ? Content.success : Content.failed;
    }

    //    根据传入的文件realPath使其count + 1
    public int updateFileCount(String realPath) {
        return fileMapper.updateFileCount(realPath);
    }

    //    返回文件真实地址
    public String getIoFilePath(String fileId, String userId) {
        return fileMapper.getIoFilePath(fileId, userId);
    }

    public UserFile getFileRealPath(String fileId) {
        return fileMapper.getFileRealPath(fileId);
    }
}
