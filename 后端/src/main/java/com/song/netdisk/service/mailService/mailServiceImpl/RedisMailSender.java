package com.song.netdisk.service.mailService.mailServiceImpl;

import com.song.netdisk.service.RedisServer;
import com.song.netdisk.service.mailService.MailSender;
import com.song.netdisk.util.Tool;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service("redisMailSender")
public class RedisMailSender extends MailSender {

    final String ttl = "ttl";

    @Resource
    private RedisServer redisServer;

    private void setCode(String address){
//        首次发送验证码
        if(!redisServer.exists(address)){
            redisServer.set(address, Tool.getRandomCode(), 1L,TimeUnit.MINUTES);
            redisServer.set(address+ttl, 0, 1L, TimeUnit.DAYS);
        }
        else{
            int times = (int) redisServer.get(address + ttl);
            if(times < 10){
                redisServer.set(address+ttl, ++times, 1L, TimeUnit.DAYS);
                redisServer.set(address, Tool.getRandomCode(), 1L, TimeUnit.MINUTES);
            }
        }
    }

    @Override
    public String sendMail(String address) {
        setCode(address);
        int dailyCounts = (int) redisServer.get(address + ttl);
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(mailUsername);
        mailMessage.setTo(address);
        mailMessage.setSubject("关于邮箱验证");
        if(dailyCounts == 10){
            mailMessage.setText("验证次数已达上限。请于24小时后再次尝试");
        }else {
            String code = (String) redisServer.get(address);
            mailMessage.setText("本次的验证码为: " + code + " , 请在1分钟内完成验证");
        }
        mailSender.send(mailMessage);
        return "邮件已发送";
    }

    @Override
    public boolean verifyCode(String code, String address) {
        String dbCode = (String) redisServer.get(address);
        return dbCode != null && dbCode.equals(code);
    }

    @Override
    public Integer getMailCounts(String address) {
        return (Integer) redisServer.get(address + ttl);
    }
}
