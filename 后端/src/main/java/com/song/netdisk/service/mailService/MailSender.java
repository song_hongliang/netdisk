package com.song.netdisk.service.mailService;

import com.song.netdisk.mapper.UserMapper;
import com.song.netdisk.util.JwtUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;

import javax.annotation.Resource;

public abstract class MailSender {

    @Value("${spring.mail.username}")
    protected String mailUsername;

    @Resource
    protected JavaMailSender mailSender;

    @Resource
    private UserMapper userMapper;

    public abstract String sendMail(String address);

    public abstract boolean verifyCode(String code, String address);

    public abstract Integer getMailCounts(String address);

    public String adminToken(String code, String address){
        if (verifyCode(code, address)) {
            String username = userMapper.getUsername(address);
            return JwtUtil.createToken(username, address);
        }
        return null;
    }
}
