package com.song.netdisk.model;

public class Content {

    //    user level
    public static Integer normalUser = 0;
    public static Integer admin = 1;

    //    filetype
    public static String file = "file";
    public static String folder = "folder";

    //    filedel
    public static String delFile = "del";
    public static String saveFile = "save";
    public static String rootFile = "root";

    //  service
    public static String success = "success";
    public static String failed = "failed";
}
