package com.song.netdisk.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    //    用户编号
    private String id;

    //    用户名
    private String username;

    //    密码
    private String password;

    //    类别。 0表示一般用户 1表示管理员
    private Integer level;
}
