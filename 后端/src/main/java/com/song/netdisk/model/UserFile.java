package com.song.netdisk.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserFile {

    //    文件编号
    private String id;

    //    文件名
    private String filename;

    //    文件路径（仅供展示用）
//    文件实际的存储路径为用户id目录下直接存储。
    private String filePath;

//    真实存储地址
    private String realPath;

//    文件创建时间
    @JsonFormat(pattern = "yyyy年 MM月 dd日 hh:mm:ss", timezone = "GMT+8")
    private Date createTime;

//    文件存在数量（分享链接造成多文件）
    private Integer fileNum;

//    文件是否公开
    private String openFile;

//    文件公开时间
    @JsonFormat(pattern = "yyyy年 MM月 dd日 hh:mm:ss", timezone = "GMT+8")
    private Date openTime;

    //    文件所有者
    private String fileOwner;

//    文件所有者昵称
    private String fileOwnerName;

    //    文件格式
    private String fileType;

    //    是否删除
    private String del;

    //   是否存储在根目录
    private Boolean saveRoot;

}
