package com.song.netdisk.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Share {

    //    分享编号
    private String id;

    //    分享者
    private String shareMan;

    //    被分享的文件编号
    private String shareFileId;

    //    分享链接有效提取次数！
    private Integer shareT;

    //    分享密码
    private String shareP;
}
