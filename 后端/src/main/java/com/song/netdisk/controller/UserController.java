package com.song.netdisk.controller;

import com.song.netdisk.model.User;
import com.song.netdisk.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;


    @GetMapping("/getUsername/{id}")
    public String getUsername(@PathVariable("id") String id){
        return userService.getUsername(id);
    }

    @PostMapping("/login")
    public Map<String, String> login(@RequestBody User user){
        return userService.login(user);
    }

    @PostMapping("/regist")
    public String regist(@RequestBody User user){
        return userService.regist(user);
    }

}
