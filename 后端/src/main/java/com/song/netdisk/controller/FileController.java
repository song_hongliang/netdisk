package com.song.netdisk.controller;

/*
 * 关于文件的open
 * 现认为只有原持有者具有open的权限
 * */

import com.song.netdisk.model.UserFile;
import com.song.netdisk.service.FileService;
import com.song.netdisk.util.Tool;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.util.List;

@RestController
@RequestMapping("/file")
public class FileController {

    @Resource
    private FileService fileService;

    //    新建文件夹专用了？
    @PostMapping("/uploadFile")
    public String uploadFile(@RequestBody UserFile userFile, ServletRequest request) {
        userFile.setFileOwner(Tool.getUserId(request));
        return fileService.uploadFile(userFile);
    }

    //    删除文件(包含批量删除)(这个是真的删掉了) 传入的参数为文件编号
    @PostMapping("/delFile")
    public String delFile( @RequestBody List<UserFile> userFiles, ServletRequest request) {
        return fileService.delFile(userFiles, Tool.getUserId(request));
    }

    //    还原文件
    @GetMapping("/livFile/{fileId}")
    public String livFile(@PathVariable("fileId") String fileId, ServletRequest request) {
        return fileService.livFile(fileId, Tool.getUserId(request));
    }

    //    查看目录下的全部文件
    @GetMapping("/view")
    public List<UserFile> getCurrentPositionFile(String path, ServletRequest request) {
        return fileService.getCurrentPositionFile(Tool.getUserId(request), path);
    }

    //    修改文件的保存状态， id为文件编号
    @PostMapping("/changeFile/{id}/{msg}")
    public String changeDel(@PathVariable("id") String id, @PathVariable("msg") String msg) {
        return fileService.changeDel(id, msg);
    }

    //    接收上传的文件
    @PostMapping("/getFile")
    public String receiveFileAndForm(@RequestParam("file") MultipartFile multipartFile, UserFile userFile, ServletRequest request) {
        userFile.setFileOwner(Tool.getUserId(request));
        return fileService.receiveFileAndForm(multipartFile, userFile);
    }

    //    接收通过分享码拿到的文件
    @PostMapping("/saveCodeFile")
    public String saveCodeFile(@RequestBody UserFile userFile, ServletRequest request) {
        return fileService.saveCodeFile(userFile, request);
    }

    //    搜索文件
    @PostMapping("/search")
    public List<UserFile> searchUserFile(@RequestBody UserFile userFile) {
        return fileService.searchUserFile(userFile.getFileOwner(), userFile.getFilename());
    }

    //    删除文件
    @PostMapping("/fakeDelFile")
    public String fakeDelFile(@RequestBody List<String> ids) {
        return fileService.fakeDel(ids);
    }

    //    查看回收站文件(根据传入的所有者编号)
    @GetMapping("/getDelFile")
    public List<UserFile> getDelFile(ServletRequest request) {
        return fileService.getDelFile(Tool.getUserId(request));
    }

    //    显示用户全部文件夹
    @GetMapping("/getUserFolder")
    public List<UserFile> getAllFolder(ServletRequest request) {
        return fileService.getAllUserFolder(Tool.getUserId(request));
    }

    //    保存一个通过提取码拿到的文件
    @PostMapping("/saveShareFile")
    public String saveShareFile(@RequestBody UserFile userFile, ServletRequest request) {
        userFile.setFileOwner(Tool.getUserId(request));
        return fileService.saveShareFile(userFile);
    }

    //    根据文件编号返回文件真实路径
    @PostMapping("/getFileRealPath")
    public UserFile getFileRealPath(@RequestBody String fileId){
        return fileService.getFileRealPath(fileId.replace("=", ""));
    }

}
