package com.song.netdisk.controller;

import com.song.netdisk.model.Share;
import com.song.netdisk.service.ShareService;
import com.song.netdisk.util.Tool;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/share")
public class ShareController {

    @Resource
    private ShareService shareService;

    @PostMapping("/create")
    public Share createShare(@RequestBody Share share, ServletRequest request) {
        share.setShareMan(Tool.getUserId(request));
        return shareService.createShare(share);
    }

    @GetMapping("/getUserShare")
    public List<Share> getUserShare(ServletRequest request) {
        return shareService.getUserShare(Tool.getUserId(request));
    }

    @PostMapping("/delShare")
    public String delShare(@RequestBody List<String> ids) {
        return shareService.delShare(ids);
    }

    @GetMapping("/getShareByFile/{fileId}")
    public Share getShareByFile(@PathVariable("fileId") String fileId) {
        return shareService.getShareByFile(fileId);
    }


    //    返回文件信息，有效提取次数。如果是文件持有者则返回文件所在路径
    @PostMapping("/getFileByShare")
    public Map<String, Object> getFileByShare(@RequestBody Share share, ServletRequest request) {
        return shareService.getFileByShare(share, Tool.getUserId(request));
    }
}
