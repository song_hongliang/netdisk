package com.song.netdisk.controller;

import com.song.netdisk.service.AdminService;
import com.song.netdisk.service.mailService.MailSender;
import com.song.netdisk.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/mail")
public class MailController {

    @Value("${adminPost}")
    protected String adminPost;

    @Autowired
    @Qualifier("redisMailSender")
    private MailSender mailSender;

    @Resource
    private AdminService adminService;

    /*
    * admin请求验证mail
    * */
    @GetMapping("/sendAdminMail")
    public String sendAdminMail(){
        return mailSender.sendMail(adminPost);
    }

    /*
    * 通过验证的admin返回token
    * */
    @GetMapping("/verifyAdminCode/{code}")
    public String verifyAdminCode(@PathVariable("code") String code){
        adminService.addAdmin(adminPost, JwtUtil.getSignature());
        return mailSender.adminToken(code, adminPost);
    }

    /*
    * 获取用户已发送的邮件次数
    * */
    @GetMapping("/getAdminMailCounts")
    public Integer getAdminMailCounts(){
        return mailSender.getMailCounts(adminPost);
    }
}
