package com.song.netdisk.controller;

import com.song.netdisk.model.UserFile;
import com.song.netdisk.service.FileIoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
/*
* 这边前端变量名叫 filePath就直接传了。实际上值为realPath
* */
@RestController
@RequestMapping("/io")
public class FileIoController {

    @Resource
    private FileIoService fileIoService;

    @PostMapping("/getFileIo")
    public void getWord(@RequestBody UserFile userFile, ServletResponse response) {
        fileIoService.getFileIo(userFile.getFilePath(), response);
    }

    @PostMapping("/word2pdf")
    public String wordTopdf(@RequestBody UserFile userFile, ServletRequest request) {
        return fileIoService.wordToPdf(userFile.getFilePath(), request);
//        return fileIoService.docToPdf(userFile.getFilePath(), request);
    }

    @PostMapping("/ppt2pdf")
    public String ppt_xToPdf(@RequestBody UserFile userFile, ServletRequest request) {
        return fileIoService.ppt_xToPdf(userFile.getFilePath(), request);
    }
}
