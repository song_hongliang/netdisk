package com.song.netdisk.controller;
/*
* 管理员专用controller
* */
import com.song.netdisk.model.UserFile;
import com.song.netdisk.service.AdminService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Resource
    private AdminService adminService;

//    获取全部文件列表
    @GetMapping("/getAllFiles")
    public List<UserFile> getAllFiles(ServletRequest request){
        return adminService.getAllFiles(request);
    }

    @GetMapping("/deleteFile/{fileId}")
    public String deleteUserFile(@PathVariable("fileId") String fileId, ServletRequest request){
        return adminService.deleteFile(fileId, request);
    }
}
