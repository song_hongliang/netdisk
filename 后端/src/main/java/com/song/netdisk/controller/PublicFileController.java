package com.song.netdisk.controller;

import com.song.netdisk.model.UserFile;
import com.song.netdisk.service.PublicFileService;
import com.song.netdisk.util.Tool;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.util.List;

@RestController
@RequestMapping("/public")
public class PublicFileController {

    @Resource
    private PublicFileService publicFileService;

    @GetMapping("/getAll")
    public List<UserFile> getAllPublicFile(){
        return publicFileService.getAllPublicFile();
    }

    @PostMapping("/openFile/{fileId}")
    public String openFile(@PathVariable("fileId")String fileId, ServletRequest request){
        String userId = Tool.getUserId(request);
        return publicFileService.openFile(fileId, userId);
    }

    @GetMapping("/getMyFile")
    public List<UserFile> getMyFile(ServletRequest request){
        return publicFileService.getMyFile(Tool.getUserId(request));
    }

    @GetMapping("/getRecentlyFile")
    public List<UserFile> getRecentlyFile(){
        return publicFileService.getRecentlyOpenFile();
    }

    @GetMapping("/search/{msg}")
    public List<UserFile> searchPublicFile(@PathVariable("msg") String msg){
        return publicFileService.searchPublicFile(msg);
    }

    @PostMapping("/close")
    public String closeFile(@RequestBody UserFile userFile, ServletRequest request){
        return publicFileService.closeFile(userFile.getId(), Tool.getUserId(request));
    }
}
