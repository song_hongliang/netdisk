package com.song.netdisk.util;

import javax.servlet.ServletRequest;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class Tool {

    public static String getUUid(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String getRandomCode(){
        return String.valueOf((int)((Math.random()*9+1) * 100000));
    }

    public static String HidPass(String password) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        BigInteger bigInteger = new BigInteger(md5.digest(password.getBytes()));
        return bigInteger.toString(16);
    }

    public static String getUserId(ServletRequest request){
        return (String) request.getAttribute("userid");
    }
}
