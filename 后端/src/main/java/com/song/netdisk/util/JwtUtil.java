package com.song.netdisk.util;

import io.jsonwebtoken.*;

import java.util.Date;
import java.util.UUID;

public class JwtUtil {

    private static long time = 1000*60*60*2;
    private static long fileAccessTime = 1000*60*60;
    private static String signature = "songhongliang";

    public static String getSignature(){
        return signature;
    }

    public static String createToken(String username, String id){
        JwtBuilder jwtBuilder = Jwts.builder();
        return jwtBuilder
                //header
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("alg", "HS256")
                //payload
                .claim("id", id)
                .claim("username", username)
                .setSubject("普通用户")
                .setExpiration(new Date(System.currentTimeMillis()+time))
                .setId(UUID.randomUUID().toString())
                //signature
                .signWith(SignatureAlgorithm.HS256, signature)
                .compact();
    }

    public static boolean checkToken(String token){
        if(token == null)
            return false;
        try{
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(signature).parseClaimsJws(token);
            Claims body = claimsJws.getBody();
            Date expiration = body.getExpiration();
            if(expiration.before(new Date()))
                return false;
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public static String getUserId(String token){
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(signature).parseClaimsJws(token);
        Claims body = claimsJws.getBody();
        return (String) body.get("id");
    }

//    设置文件的有效访问时长
    public static String setFileAccess(String realPath){
        JwtBuilder jwtBuilder = Jwts.builder();
        return jwtBuilder
                //header
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("alg", "HS256")
                .setSubject(realPath)
                .setExpiration(new Date(System.currentTimeMillis()+fileAccessTime))
                .setId(UUID.randomUUID().toString())
                //signature
                .signWith(SignatureAlgorithm.HS256, signature)
                .compact();
    }
}
