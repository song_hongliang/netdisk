package com.song.netdisk.config;

import com.song.netdisk.util.JwtUtil;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class TokenFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) response;

        HttpServletRequest req = (HttpServletRequest) request;
        String requestURI = ((HttpServletRequest) request).getRequestURI();

//        请求实际文件，需要单独处理
        if(requestURI.contains("/diskFile/") && !req.getMethod().equals("OPTIONS")){
            String access = request.getParameter("access");
            if(access == null || access.isEmpty()){
//                预览文件，直接允许
                if(req.getRequestURI().contains("view.pdf"))
                    filterChain.doFilter(req, resp);
                else
                    return;
            }
            if(!Objects.equals(access, "")){
                if(JwtUtil.checkToken(access)){
                    filterChain.doFilter(req, resp);
                }
                else{
                    resp.setHeader("Access-Control-Allow-Origin", "*");
                    resp.setHeader("Cache-Control", "no-cache");
                    resp.setCharacterEncoding("UTF-8");
                    resp.setContentType("application/json");
                    resp.getWriter().write("链接失效!");
                }
                return;
            }
        }

        List<String> exceptURI = new ArrayList<>();
        exceptURI.add("/user/");
        exceptURI.add("error");
        exceptURI.add("swagger");
        exceptURI.add("/mail");
        exceptURI.add("api-docs");
//        exceptURI.add("/diskFile/");
        long count = exceptURI.stream().filter(requestURI::contains).count();
        if (count == 0) {
            String header = req.getHeader("Authorization");
            if (!JwtUtil.checkToken(header)) {
                if(!req.getMethod().equals("OPTIONS")){
                    resp.setHeader("Access-Control-Allow-Origin", "*");
                    resp.setHeader("Cache-Control", "no-cache");
                    resp.setCharacterEncoding("UTF-8");
                    resp.setContentType("application/json");
                    resp.getWriter().write("未通过登陆验证!");
                    return;
                }

            }else{
                req.setAttribute("userid", JwtUtil.getUserId(header));
            }
        }
        filterChain.doFilter(req, resp);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
