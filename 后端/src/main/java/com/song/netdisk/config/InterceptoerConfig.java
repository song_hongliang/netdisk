package com.song.netdisk.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptoerConfig implements WebMvcConfigurer {

    @Value(("${savePath}"))
    private String path;

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/diskFile/**").addResourceLocations("file:" + path + "/");
    }
}
