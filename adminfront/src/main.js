import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import config from './config/config.js'
import axios from 'axios'

axios.interceptors.request.use(config => {
  let url = config.url;
  let str = localStorage.getItem("adminToken");
  if (str == null || str == '') {
    if (url.indexOf("mail") != -1) 
      return config;
    return router.push("/");
  }
  let token = localStorage.getItem("adminToken");
  config.headers.Authorization = token;
  return config;
})

axios.interceptors.response.use(config => {
  let resp = config;
  if (resp.data == null) {
    alert("登录失效");
    localStorage.clear();
    return router.push("/");
  }
  return config;
})

Vue.use(config);
Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$axios = axios;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
