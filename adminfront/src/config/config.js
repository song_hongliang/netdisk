export default{
    install(Vue){
        // 开发环境
        // Vue.prototype.apiUrl = "http://localhost:8081";
        // Vue.prototype.diskFileUrl = "http://localhost:8081/diskFile/"
        
        // 部署运行环境
        Vue.prototype.apiUrl = "http://42.192.151.76:8081";
        Vue.prototype.diskFileUrl = "http://42.192.151.76:8081/diskFile/"

        Vue.prototype.resSuccess = "success";
        Vue.prototype.resFailed = "failed";
    }
}
