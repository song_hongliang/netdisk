import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import File from '../views/Files.vue'

Vue.use(VueRouter)

//获取原型对象上的push函数
const originalPush = VueRouter.prototype.push
//修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/files',
    name: 'Files',
    component: File,
  }
]

const router = new VueRouter({
  routes
})

export default router
