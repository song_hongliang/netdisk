import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import Files from '../views/Files.vue';
import DelFile from '../views/DelFile.vue';
import OpFile from '../views/OpFile.vue';
import Menu from '../components/Menu.vue';
import PagedPlayer from '../views/PagedPlayer.vue';
import Zone from '../views/Zone.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/menu',
    name: 'Menu',
    component: Menu,
    children: [
      {
        path: 'file',
        name: 'File',
        component: Files
      },
      {
        path: 'opFile',
        name: 'OpFile',
        component: OpFile
      },
      {
        path: 'delFile',
        name: 'DelFile',
        component: DelFile
      }
    ]
  },
  {
    path: '/pagePlayer/:id',
    name: 'pagePlayer',
    component: PagedPlayer
  },
  {
    path: '/zone',
    name: 'zone',
    component: Zone,
  }


]

const router = new VueRouter({
  routes
})

export default router
