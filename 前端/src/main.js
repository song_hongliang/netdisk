import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios';
import config from './config/config.js'

Vue.config.productionTip = false
Vue.prototype.$axios = axios;
Vue.use(config);


const logintUrl = Vue.prototype.apiUrl + '/user/login';
const registUrl = Vue.prototype.apiUrl + '/user/regist';

axios.interceptors.request.use( config => {
  let url = config.url;
  if (url != logintUrl && url != registUrl) {
    if (!localStorage.getItem("token")) {
      if(url.indexOf("regist") != -1 || url.indexOf("login") != -1){
        console.log("登陆与注册，啥都不干");
        return;
      }
      return router.push("/");
    }
    let token = localStorage.getItem("token");
    config.headers.Authorization = token;
  }
  return config;
})

axios.interceptors.response.use(config=>{
  let resp = config;
  if(resp.data == '未通过登陆验证!'){
    alert("登录已失效！");
    localStorage.clear();
    return router.push("/");
  }
  return config;
})

Vue.use(ElementUI);


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
