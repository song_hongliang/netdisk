## 关于文件路径
    点击时判断点击的文件类型，为文件夹则修改路径并进行跳转~~（文件，文件夹的名字不能包含 / ）~~
    返回上一级功能需要修改的是路径

## 关于word的在线预览（但是好像不行）
    https://www.microsoft.com/en-us/microsoft-365/blog/2013/04/10/office-web-viewer-view-office-documents-in-a-browser/
    https://www.cnblogs.com/tomiaa/p/15048829.html
    现在使用了word-preview组件但是改了包文件。不知道为啥JSZip引入不行了

    改了。现在后端把word转pdf然后给前端看了

## 关于ppt的预览
    后端用了个spire的包。但是是免费的目前还不知道会出来啥问题

## 关于xlsx的预览
    后端传文件流前端用xlsx查看。目前只能看到表格数据

## 关于文件公开
    初步计划为： 只有源文件持有者可以公开手上的文件。
    目前打算进行内部划分区域，分为视频，音频，其他三个区域。
    搜索。文件有点多最好有个搜索功能不然不太好找

## 关于个人空间
    随便整一个就得了，主要是要体现撤回功能